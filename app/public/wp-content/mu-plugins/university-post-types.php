<?php
function university_post_types (){
    //Event post type 
    register_post_type('event', array(
        'public' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'custom-fields'
        ),
        'rewrite' => array (
            'slug' => 'events'
        ),
        'show_in_rest' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-calendar',
        'labels' => array(
            'name' => 'Events',
            'add_new_item' => 'Add New Event',
            'edit_item' => 'Edit Event',
            'all_items' => 'All Events',
            'singular_name' => 'Events›'
        )

    ));
    // Program post type

    register_post_type('program', array(
        'public' => true,
        'supports' => array(
            'title',
            'editor',
            'custom-fields'
        ),
        'rewrite' => array (
            'slug' => 'programs'
        ),
        'show_in_rest' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-awards',
        'labels' => array(
            'name' => 'Programs',
            'add_new_item' => 'Add New Program',
            'edit_item' => 'Edit Program',
            'all_items' => 'All Programs',
            'singular_name' => 'Programs'
        )

    ));

}
add_action('init', 'university_post_types');
?>